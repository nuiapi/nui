
#include <cppbuild.hpp>

int main(int argc, char* argv[])
{
    cppbuild::init(argc, argv);
    
    cppbuild::Target target("nui", cppbuild::TargetTypes::StaticLibrary);

    target.folder("src", {
        "nui-win32.cpp",
        "flui-nui.cpp", 
        "glextl.cpp",
    });

    target.includeDirs({
        "include",
        "%WIN_CPP_PREFIX_PATH%\\include"
    });

    target.installDir("%WIN_CPP_PREFIX_PATH%");
    target.installTargetInto("lib");
    target.installFiles("include", {
        "include\\flui-nui.h",
        "include\\nui.h",
    });

    return 0;
}
