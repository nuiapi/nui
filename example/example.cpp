
#include <flui-nui.h>
#include <nui.h>
#include <sstream>

extern const wchar_t MainWidget[] = L"MainWidget";
extern const wchar_t click_me[] = L"click_me";
extern const wchar_t click_me_2[] = L"click_me_2";
extern const wchar_t auw[] = L"auw";
extern const wchar_t auwts[] = L"auwts";
extern const wchar_t more[] = L"more";

class ExampleController : public nui::Controller
{
public:
    void OnClick(nui::event::EventArgs *e);

    void OnClickAuw(nui::event::EventArgs *e);

    void OnResize(nui::event::ResizeEventArgs *args);
};

void ExampleController::OnClick(
    nui::event::EventArgs *args)
{
    args->Sender()->Text(L"ojee");
}

void ExampleController::OnClickAuw(
    nui::event::EventArgs *args)
{
    auto btn = args->Sender()->Parent()->FindChildById<nui::Button>(auw);

    if (btn == nullptr)
    {
        return;
    }

    btn->Text(btn->Text() + L"Auw");
}

void ExampleController::OnResize(
    nui::event::ResizeEventArgs *args)
{
    auto container = reinterpret_cast<nui::WidgetContainer *>(args->Sender());

    auto label = container->FindChildById<nui::Label>(click_me_2);

    if (label != nullptr)
    {
        std::wstringstream ss;

        ss << L"OnResize : " << args->Width() << L" - " << args->Height();
        label->Text(ss.str());
    }
}

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    using namespace nui::event;

    try
    {
        return FluiNui()
            .MainWidget<MainWidget, ExampleController>(
                L"nui MainWidget Demo",
                [&](ContainerBuilder &builder) {
                    builder
                        .FixedWidth(300)
                        .FixedHeight(200)
                        .OnResize((ResizeEventHandlerFn)&ExampleController::OnResize);

                    builder
                        .ChildArrangement(nui::ChildArrangementStyles::Horizontal)
                        .Button<click_me>(
                            L"Click Me",
                            [&](WidgetBuilder &builder) {
                                builder.OnClick((ClickEventHandlerFn)&ExampleController::OnClick);
                            })
                        .Container<more>(
                            [](ContainerBuilder &builder) {
                                builder
                                    .ChildArrangement(nui::ChildArrangementStyles::Vertical)
                                    .Button<auw>(L"Auw")
                                    .Button<auwts>(
                                        L"Auw ts",
                                        [&](WidgetBuilder &builder) {
                                            builder.OnClick((ClickEventHandlerFn)&ExampleController::OnClickAuw);
                                        });
                            })
                        .Label<click_me_2>(L"Hell\xF6\x0A");
                })
            .Run();
    }
    catch (const std::exception &ex)
    {
        std::string cs(ex.what());
        std::wstring ws;
        std::copy(cs.begin(), cs.end(), std::back_inserter(ws));

        nui::Application::MessagePopUp(ws);

        return 0;
    }
}
