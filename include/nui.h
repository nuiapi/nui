#ifndef NUI_H
#define NUI_H

#include <set>
#include <string>
#include <vector>

#if defined(_WIN32)
struct HWND__;
typedef void *PVOID;
typedef PVOID HANDLE;
#endif

namespace nui
{
    class Widget;
    class Application;

    class Controller
    {
    public:
        virtual ~Controller() {}
    };

    namespace event
    {
// Start Event stuff

// Got these from: https://isocpp.org/wiki/faq/pointers-to-members
#define CALL_FNPTR_ON_OBJ(object, ptrToMember) ((object).*(ptrToMember))
#define CALL_FNPTR_ON_OBJPTR(ptrToObject, ptrToMember) (*(ptrToObject).*(ptrToMember))

        template <class T>
        class EventHandler
        {
        public:
            EventHandler(
                Controller *obj,
                T fn)
                : objPtr(obj),
                  fnPtr(fn) {}

            virtual ~EventHandler() {}

            bool operator<(
                const EventHandler &s) const
            {
                return this->objPtr < s.objPtr;
            }

            Controller *objPtr;
            T fnPtr;
        };

        template <class T, class A>
        class Event
        {
        public:
            Event &operator+=(
                const EventHandler<T> &handler)
            {
                this->handlers.insert(handler);
                return *this;
            }

            Event &operator-=(
                const EventHandler<T> &handler)
            {
                this->handlers.erase(handler);
                return *this;
            }

            void operator()(
                A *args)
            {
                for (typename HandlerSet::iterator i = this->handlers.begin(); i != this->handlers.end(); ++i)
                {
                    CALL_FNPTR_ON_OBJPTR(i->objPtr, i->fnPtr)
                    (args);
                }
            }

        private:
            typedef typename std::set<EventHandler<T>> HandlerSet;
            HandlerSet handlers;
        };

        class EventArgs
        {
        public:
            explicit EventArgs(
                Widget *sender)
                : _sender(sender)
            {}

            Widget *Sender() const
            {
                return this->_sender;
            }

        private:
            Widget *_sender;
        };

        typedef void (Controller::*EventHandlerFn)(EventArgs *);

        class ResizeEventArgs : public EventArgs
        {
        public:
            ResizeEventArgs(
                Widget *sender,
                int width,
                int height)
                : EventArgs(sender),
                  _width(width),
                  _height(height)
            {}

            ResizeEventArgs(
                const ResizeEventArgs &other)
                : EventArgs(other.Sender()),
                  _width(other.Width()),
                  _height(other.Height())
            {}

            int Width() const
            {
                return this->_width;
            }

            int Height() const
            {
                return this->_height;
            }

        private:
            int _width;
            int _height;
        };

        typedef void (Controller::*ResizeEventHandlerFn)(ResizeEventArgs *);

        typedef Event<ResizeEventHandlerFn, ResizeEventArgs> ResizeEvent;

        typedef EventHandler<ResizeEventHandlerFn> ResizeEventHandler;

        class ClickEventArgs : public EventArgs
        {
        public:
            explicit ClickEventArgs(
                Widget *sender)
                : EventArgs(sender)
            {}
        };

        typedef void (Controller::*ClickEventHandlerFn)(EventArgs *);

        typedef Event<ClickEventHandlerFn, ClickEventArgs> ClickEvent;

        typedef EventHandler<ClickEventHandlerFn> ClickEventHandler;

        class InitEventArgs : public EventArgs
        {
        public:
            explicit InitEventArgs(
                Widget *sender)
                : EventArgs(sender)
            {}
        };

        typedef void (Controller::*InitEventHandlerFn)(EventArgs *);

        typedef Event<InitEventHandlerFn, InitEventArgs> InitEvent;

        typedef EventHandler<InitEventHandlerFn> InitEventHandler;

        class IdleEventArgs : public EventArgs
        {
        public:
            explicit IdleEventArgs(
                Widget *sender)
                : EventArgs(sender)
            {}
        };

        typedef void (Controller::*IdleEventHandlerFn)(EventArgs *);

        typedef Event<IdleEventHandlerFn, IdleEventArgs> IdleEvent;

        typedef EventHandler<IdleEventHandlerFn> IdleEventHandler;

        class ExitEventArgs : public EventArgs
        {
        public:
            explicit ExitEventArgs(
                Widget *sender)
                : EventArgs(sender)
            {}
        };
        typedef void (Controller::*ExitEventHandlerFn)(EventArgs *);

        typedef Event<ExitEventHandlerFn, ExitEventArgs> ExitEvent;

        typedef EventHandler<ExitEventHandlerFn> ExitEventHandler;

        class SelectedIndexChangedEventArgs : public EventArgs
        {
        public:
            SelectedIndexChangedEventArgs(
                Widget *sender,
                int newIndex)
                : EventArgs(sender),
                  _newIndex(newIndex)
            {}

            int NewIndex() const
            {
                return this->_newIndex;
            }

        private:
            int _newIndex;
        };

        typedef void (Controller::*SelectedIndexChangedEventHandlerFn)(EventArgs *);

        typedef Event<SelectedIndexChangedEventHandlerFn, SelectedIndexChangedEventArgs> SelectedIndexChangedEvent;

        typedef EventHandler<SelectedIndexChangedEventHandlerFn> SelectedIndexChangedEventHandler;

        // End Event stuff
    } // namespace event

// Start Font stuff
#if defined(_WIN32)
    typedef HANDLE FontHandle;
#endif

    namespace priv
    {
        class FontImpl;
    }

    class FontStyle
    {
    public:
        FontStyle();

        FontStyle(
            const FontStyle &style);

        FontStyle(
            const std::wstring &name,
            int size,
            bool bold = false,
            bool italic = false,
            bool underline = false,
            bool striketrough = false);

        virtual ~FontStyle();

        FontStyle &operator=(
            const FontStyle &other);

        FontHandle GetFontHandle() const;

        std::wstring Name() const;

        int Size() const;

        bool Bold() const;

        bool Italic() const;

        bool Underline() const;

        bool StrikeTrough() const;

    private:
        priv::FontImpl *_impl;
    };

    // End Font stuff

#if defined(_WIN32)
    typedef HWND__ *WidgetHandle;
    typedef HANDLE ApplicationHandle;
#endif

    namespace priv
    {
        class ApplicationImpl;
        class WidgetImpl;
        class WidgetContainerImpl;
    } // namespace priv

    class Application;
    class WidgetContainer;

    class Widget
    {
    public:
        virtual ~Widget();

        Application *App() const;

        WidgetHandle Handle() const;

        WidgetContainer *Parent() const;

        virtual const Controller *LinkedController() const;

        virtual Controller *LinkedController();

        event::ResizeEvent OnResize;

        const std::wstring &Id() const;

        std::wstring Text() const;

        void Text(
            const std::wstring &text);

        void Font(
            const FontStyle *font);

        bool IsWidthFixed() const;

        void SetWidthFixed(
            bool fixed);

        int Width() const;

        void Width(
            int width);

        bool IsHeightFixed() const;

        void SetHeightFixed(
            bool fixed);

        int Height() const;

        void Height(
            int height);

    protected:
        friend class Application;

        Widget(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);

    protected:
        priv::WidgetImpl *_impl;
        Application *_application;
        WidgetContainer *_parent;
    };

    namespace ChildArrangementStyles
    {
        enum eType
        {
            Free,
            Horizontal,
            Vertical
        };
    }

    class WidgetContainer : public Widget
    {
    public:
        virtual ~WidgetContainer();

        virtual const Controller *LinkedController() const;

        virtual Controller *LinkedController();

        const std::vector<Widget *> &Children() const;

        std::vector<Widget *> &Children();

        void AddChild(
            Widget *widget);

        void RemoveChild(
            Widget *widget);

        Widget *FindChildById(
            const std::wstring &id);

        template <class TWidget>
        TWidget *FindChildById(
            const std::wstring &id)
        {
            return reinterpret_cast<TWidget *>(FindChildById(id));
        }

        ChildArrangementStyles::eType ChildArrangementStyle();

        void ChildArrangementStyle(
            ChildArrangementStyles::eType style);

        int *Padding() const;

        void Padding(
            int padding);

        void Padding(
            int horizontal,
            int vertical);

        void Padding(
            int left,
            int top,
            int right,
            int bottom);

    protected:
        friend class Application;

        WidgetContainer(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);

        WidgetContainer(
            Controller *linkedController,
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);

    private:
        Controller *_linkedController;
        std::vector<Widget *> _children;
    };

    class GlWidget : public Widget
    {
    public:
        virtual ~GlWidget();

        bool SetupModernGl(
            int major,
            int minor);

        void BeginFrame();

        void EndFrame();

    protected:
        friend class Application;

        GlWidget(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);
    };

    class Label : public Widget
    {
    public:
        virtual ~Label();

    protected:
        friend class Application;

        Label(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);
    };

    class Button : public Widget
    {
    public:
        virtual ~Button();

        event::ClickEvent OnClick;

    protected:
        friend class Application;

        Button(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);
    };

    class RadioButton : public Widget
    {
    public:
        virtual ~RadioButton();

        bool IsChecked() const;

        void Check(
            bool enabled);

        event::ClickEvent OnClick;

    protected:
        friend class Application;

        RadioButton(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);
    };

    class TextBox : public Widget
    {
    public:
        virtual ~TextBox();

    protected:
        friend class Application;

        TextBox(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);
    };

    class CheckBox : public Widget
    {
    public:
        virtual ~CheckBox();

        bool IsChecked() const;

        void Check(
            bool enabled);

        event::ClickEvent OnClick;

    protected:
        friend class Application;

        CheckBox(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);
    };

    class ComboBox : public Widget
    {
    public:
        enum eType
        {
            Simple,
            DropDown,
            DropDownList
        };

    public:
        virtual ~ComboBox();

        eType ListType() const;

        void ListType(
            eType type);

        void SetList(
            const std::vector<std::wstring> &items);

        void AddListItem(
            const std::wstring &text);

        void InsertListItem(
            int index,
            const std::wstring &text);

        void RemoveListItem(
            int index);

        void ClearItems();

        int IndexOf(
            const std::wstring &text,
            int from = 0);

        int SelectedIndex() const;

        std::wstring SelectedText() const;

        event::SelectedIndexChangedEvent OnSelectedIndexChanged;

    protected:
        friend class Application;

        ComboBox(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            ComboBox::eType type,
            priv::WidgetImpl *impl = 0);
    };

    class ListBox : public Widget
    {
    public:
        virtual ~ListBox();

        void SetList(
            const std::vector<std::wstring> &items);

        void AddListItem(
            const std::wstring &text);

        void InsertListItem(
            int index,
            const std::wstring &text);

        void RemoveListItem(
            int index);

        void ClearItems();

        int IndexOf(
            const std::wstring &text,
            int from = 0);

        int SelectedIndex() const;

        std::wstring SelectedText() const;

        event::SelectedIndexChangedEvent OnSelectedIndexChanged;

    protected:
        friend class Application;

        ListBox(
            const std::wstring &id,
            Application *app,
            WidgetContainer *parent,
            priv::WidgetImpl *impl = 0);
    };

    class Application
    {
    public:
        Application(
            ApplicationHandle handle = 0);

        virtual ~Application();

    public:
        virtual int Run();

        ApplicationHandle GetApplicationHandle();

        Widget *Find(
            const std::wstring &id);

        event::InitEvent OnInit;

        event::IdleEvent OnIdle;

        event::ExitEvent OnExit;

        static FontStyle *_defaultFont;

    public:
        WidgetContainer *CreateTopWidget(
            nui::Controller *controller,
            const std::wstring &id);

        WidgetContainer *CreateChildContainer(
            nui::Controller *controller,
            const std::wstring &id,
            WidgetContainer *parent);

        Widget *CreateChildWidget(
            const std::wstring &id,
            WidgetContainer *parent);

        GlWidget *CreateGlWidget(
            const std::wstring &id,
            WidgetContainer *parent);

        Label *CreateLabel(
            const std::wstring &id,
            WidgetContainer *parent);

        Button *CreateButton(
            const std::wstring &id,
            WidgetContainer *parent);

        RadioButton *CreateRadioButton(
            const std::wstring &id,
            WidgetContainer *parent);

        CheckBox *CreateCheckBox(
            const std::wstring &id,
            WidgetContainer *parent);

        TextBox *CreateTextBox(
            const std::wstring &id,
            WidgetContainer *parent);

        ComboBox *CreateComboBox(
            const std::wstring &id,
            WidgetContainer *parent,
            ComboBox::eType type = ComboBox::DropDownList);

        ListBox *CreateListBox(
            const std::wstring &id,
            WidgetContainer *parent);

        static void MessagePopUp(
            const std::wstring &message);

    private:
        priv::ApplicationImpl *_impl;
    };

} // namespace nui

#endif // NUI_H
