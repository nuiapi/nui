#ifndef FLUINUI_H
#define FLUINUI_H

#include "nui.h"

#include <functional>

class WidgetBuilder
{
public:
    // Properties
    WidgetBuilder &Text(
        const std::wstring &text);

    WidgetBuilder &FixedWidth(
        int width);

    WidgetBuilder &FixedHeight(
        int height);

    WidgetBuilder &Font(
        nui::FontStyle *font);

    // Events
    WidgetBuilder &FireEvent(
        const nui::event::EventHandlerFn &handler);

    WidgetBuilder &OnResize(
        const nui::event::ResizeEventHandlerFn &handler);

    WidgetBuilder &OnClick(
        const nui::event::ClickEventHandlerFn &handler);

    WidgetBuilder &OnSelectedIndexChanged(
        const nui::event::SelectedIndexChangedEventHandlerFn &handler);

protected:
    WidgetBuilder(
        nui::Application *app,
        nui::Widget *widget);

    nui::Application *_app;
    nui::Widget *_lastCreated;

    friend class FluiNui;
    friend class ContainerBuilder;
};

class ContainerBuilder : public WidgetBuilder
{
public:
    // Widget Creation
    template <const wchar_t *id>
    ContainerBuilder &Container(
        std::function<void(ContainerBuilder &)> builder = {})
    {
        return Container(id, builder);
    }

    ContainerBuilder &Container(
        const std::wstring &id,
        std::function<void(ContainerBuilder &)> builder = {});

    ContainerBuilder &Spacer();

    template <const wchar_t *id>
    ContainerBuilder &Label(
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return Label(id, text, builder);
    }

    ContainerBuilder &Label(
        const std::wstring &id,
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &Button(
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return Button(id, text, builder);
    }

    ContainerBuilder &Button(
        const std::wstring &id,
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &RadioButton(
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return RadioButton(id, text, builder);
    }

    ContainerBuilder &RadioButton(
        const std::wstring &id,
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &CheckBox(
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return CheckBox(id, text, builder);
    }

    ContainerBuilder &CheckBox(
        const std::wstring &id,
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &TextBox(
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return TextBox(id, text, builder);
    }

    ContainerBuilder &TextBox(
        const std::wstring &id,
        const std::wstring &text,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &DropDown(
        const std::vector<std::wstring> &items,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return DropDown(id, items, builder);
    }

    ContainerBuilder &DropDown(
        const std::wstring &id,
        const std::vector<std::wstring> &items,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &DropDownList(
        const std::vector<std::wstring> &items,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return DropDownList(id, items, builder);
    }

    ContainerBuilder &DropDownList(
        const std::wstring &id,
        const std::vector<std::wstring> &items,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &ListBox(
        const std::vector<std::wstring> &items,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return ListBox(id, items, builder);
    }

    ContainerBuilder &ListBox(
        const std::wstring &id,
        const std::vector<std::wstring> &items,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &Gl(
        int major = 2,
        int minor = 1,
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return Gl(id, major, minor, builder);
    }

    ContainerBuilder &Gl(
        const std::wstring &id,
        int major = 2,
        int minor = 1,
        std::function<void(WidgetBuilder &)> builder = {});

    template <const wchar_t *id>
    ContainerBuilder &GlLatest(
        std::function<void(WidgetBuilder &)> builder = {})
    {
        return GlLatest(id, builder);
    }

    ContainerBuilder &GlLatest(
        const std::wstring &id,
        std::function<void(WidgetBuilder &)> builder = {});

    // Properties
    ContainerBuilder &ChildArrangement(
        nui::ChildArrangementStyles::eType arrangement);

    ContainerBuilder &Padding(
        int padding);

    ContainerBuilder &Padding(
        int horizontal,
        int vertical);

    ContainerBuilder &Padding(
        int left,
        int top,
        int right,
        int bottom);

protected:
    ContainerBuilder(
        nui::Controller *controller,
        nui::Application *app,
        nui::WidgetContainer *container);

    ContainerBuilder(
        nui::Application *app,
        nui::WidgetContainer *container);

    nui::WidgetContainer *_currentContainer;

    friend class FluiNui;
};

class FluiNui
{
public:
    FluiNui(
        nui::ApplicationHandle handle = nullptr);

    virtual ~FluiNui();

    int Run();

    // Widget Creation
    template <const wchar_t *id>
    FluiNui &MainWidget(
        const std::wstring &text,
        std::function<void(ContainerBuilder &)> builder = {})
    {
        return MainWidget(nullptr, id, text, builder);
    }

    template <const wchar_t *id, class TController>
    FluiNui &MainWidget(
        const std::wstring &text,
        std::function<void(ContainerBuilder &)> builder = {})
    {
        auto controller = new TController();
        return MainWidget(controller, id, text, builder);
    }

    template <class TController>
    FluiNui &MainWidget(
        const std::wstring &id,
        const std::wstring &text,
        std::function<void(ContainerBuilder &)> builder = {})
    {
        auto controller = new TController();
        return MainWidget(controller, id, text, builder);
    }

    FluiNui &MainWidget(
        nui::Controller *controller,
        const std::wstring &id,
        const std::wstring &text,
        std::function<void(ContainerBuilder &)> builder = {});

    // Events
    FluiNui &OnIdle(
        const nui::event::IdleEventHandlerFn &handler);

    FluiNui &OnExit(
        const nui::event::ExitEventHandlerFn &handler);

private:
    nui::Application *_app;
    bool _appOwner;
    nui::WidgetContainer *_currentContainer;
    nui::Widget *_lastCreated;
};

#endif // FLUINUI_H
