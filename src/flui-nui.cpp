#include <flui-nui.h>
#include <sstream>
#include <stdexcept>

WidgetBuilder::WidgetBuilder(
    nui::Application *app,
    nui::Widget *widget)
    : _app(app),
      _lastCreated(widget)
{}

ContainerBuilder::ContainerBuilder(
    nui::Application *app,
    nui::WidgetContainer *container)
    : WidgetBuilder(app, container),
      _currentContainer(container)
{}

FluiNui::FluiNui(
    nui::ApplicationHandle handle)
    : _app(new nui::Application(handle)),
      _appOwner(true),
      _currentContainer(nullptr),
      _lastCreated(nullptr)
{}

FluiNui::~FluiNui()
{
    if (this->_appOwner)
    {
        delete this->_app;
    }
}

int FluiNui::Run()
{
    return this->_app->Run();
}

FluiNui &FluiNui::MainWidget(
    nui::Controller *controller,
    const std::wstring &id,
    const std::wstring &text,
    std::function<void(ContainerBuilder &)> builder)
{
    nui::WidgetContainer *c = this->_app->CreateTopWidget(controller, id);
    c->Text(text);

    this->_lastCreated = this->_currentContainer = c;

    if (builder)
    {
        ContainerBuilder cb(this->_app, this->_currentContainer);

        builder(cb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::Container(
    const std::wstring &id,
    std::function<void(ContainerBuilder &)> builder)
{
    nui::WidgetContainer *c = this->_app->CreateChildContainer(this->_currentContainer->LinkedController(), id, this->_currentContainer);

    if (builder)
    {
        ContainerBuilder cb(this->_app, c);

        builder(cb);
    }

    return *this;
}

static int spaceId = 0;
ContainerBuilder &ContainerBuilder::Spacer()
{
    std::wstringstream ss;

    ss << "nui::Spacer_" << spaceId++;

    this->_app->CreateChildWidget(ss.str(), this->_currentContainer);

    return *this;
}

ContainerBuilder &ContainerBuilder::Label(
    const std::wstring &id,
    const std::wstring &text,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::Label *c = this->_app->CreateLabel(id, this->_currentContainer);
    c->Text(text);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::Button(
    const std::wstring &id,
    const std::wstring &text,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::Button *c = this->_app->CreateButton(id, this->_currentContainer);
    c->Text(text);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::RadioButton(
    const std::wstring &id,
    const std::wstring &text,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::RadioButton *c = this->_app->CreateRadioButton(id, this->_currentContainer);
    c->Text(text);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::CheckBox(
    const std::wstring &id,
    const std::wstring &text,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::CheckBox *c = this->_app->CreateCheckBox(id, this->_currentContainer);
    c->Text(text);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::TextBox(
    const std::wstring &id,
    const std::wstring &text,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::TextBox *c = this->_app->CreateTextBox(id, this->_currentContainer);
    c->Text(text);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::DropDown(
    const std::wstring &id,
    const std::vector<std::wstring> &items,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::ComboBox *c = this->_app->CreateComboBox(id, this->_currentContainer, nui::ComboBox::DropDown);
    c->SetList(items);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::DropDownList(
    const std::wstring &id,
    const std::vector<std::wstring> &items,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::ComboBox *c = this->_app->CreateComboBox(id, this->_currentContainer, nui::ComboBox::DropDownList);
    c->SetList(items);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::ListBox(
    const std::wstring &id,
    const std::vector<std::wstring> &items,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::ListBox *c = this->_app->CreateListBox(id, this->_currentContainer);
    c->SetList(items);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::Gl(
    const std::wstring &id,
    int major,
    int minor,
    std::function<void(WidgetBuilder &)> builder)
{
    nui::GlWidget *c = this->_app->CreateGlWidget(id, this->_currentContainer);
    c->SetupModernGl(major, minor);

    if (builder)
    {
        WidgetBuilder wb(this->_app, c);

        builder(wb);
    }

    return *this;
}

ContainerBuilder &ContainerBuilder::GlLatest(
    const std::wstring &id,
    std::function<void(WidgetBuilder &)> builder)
{
    return Gl(id, 4, 6, builder);
}

FluiNui &FluiNui::OnIdle(
    const nui::event::IdleEventHandlerFn &handler)
{
    this->_app->OnIdle += nui::event::IdleEventHandler(
        this->_lastCreated->LinkedController(),
        handler);

    return *this;
}

FluiNui &FluiNui::OnExit(
    const nui::event::ExitEventHandlerFn &handler)
{
    this->_app->OnExit += nui::event::ExitEventHandler(
        this->_lastCreated->LinkedController(),
        handler);

    return *this;
}

WidgetBuilder &WidgetBuilder::OnResize(
    const nui::event::ResizeEventHandlerFn &handler)
{
    this->_lastCreated->OnResize += nui::event::ResizeEventHandler(
        this->_lastCreated->LinkedController(),
        handler);

    return *this;
}

WidgetBuilder &WidgetBuilder::OnClick(
    const nui::event::ClickEventHandlerFn &handler)
{
    nui::Button *btn = dynamic_cast<nui::Button *>(this->_lastCreated);
    nui::RadioButton *rad = dynamic_cast<nui::RadioButton *>(this->_lastCreated);
    nui::CheckBox *chk = dynamic_cast<nui::CheckBox *>(this->_lastCreated);

    if (btn != nullptr)
    {
        btn->OnClick += nui::event::ClickEventHandler(
            this->_lastCreated->LinkedController(),
            handler);
    }
    else if (rad != nullptr)
    {
        rad->OnClick += nui::event::ClickEventHandler(
            this->_lastCreated->LinkedController(),
            handler);
    }
    else if (chk != nullptr)
    {
        chk->OnClick += nui::event::ClickEventHandler(
            this->_lastCreated->LinkedController(),
            handler);
    }
    else
    {
        throw std::runtime_error("The last added widget is not a clickable type like nui::Button");
    }

    return *this;
}

WidgetBuilder &WidgetBuilder::OnSelectedIndexChanged(
    const nui::event::SelectedIndexChangedEventHandlerFn &handler)
{
    nui::ListBox *lst = dynamic_cast<nui::ListBox *>(this->_lastCreated);
    nui::ComboBox *cmb = dynamic_cast<nui::ComboBox *>(this->_lastCreated);

    if (lst != nullptr)
    {
        lst->OnSelectedIndexChanged += nui::event::SelectedIndexChangedEventHandler(
            this->_lastCreated->LinkedController(),
            handler);
    }
    else if (cmb != nullptr)
    {
        cmb->OnSelectedIndexChanged += nui::event::SelectedIndexChangedEventHandler(
            this->_lastCreated->LinkedController(),
            handler);
    }
    else
    {
        throw std::runtime_error("The last added widget is not a list type like nui::ListBox");
    }

    return *this;
}

WidgetBuilder &WidgetBuilder::FireEvent(
    const nui::event::EventHandlerFn &handler)
{
    nui::event::EventArgs args(this->_lastCreated);
    nui::event::Event<nui::event::EventHandlerFn, nui::event::EventArgs> event;

    event += nui::event::EventHandler<nui::event::EventHandlerFn>(
        this->_lastCreated->LinkedController(),
        handler);

    event(&args);

    return *this;
}

WidgetBuilder &WidgetBuilder::Text(
    const std::wstring &text)
{
    this->_lastCreated->Text(text);

    return *this;
}

WidgetBuilder &WidgetBuilder::FixedWidth(
    int width)
{
    this->_lastCreated->Width(width);

    return *this;
}

WidgetBuilder &WidgetBuilder::FixedHeight(
    int height)
{
    this->_lastCreated->Height(height);

    return *this;
}

ContainerBuilder &ContainerBuilder::ChildArrangement(
    nui::ChildArrangementStyles::eType arrangement)
{
    this->_currentContainer->ChildArrangementStyle(arrangement);

    return *this;
}

ContainerBuilder &ContainerBuilder::Padding(
    int padding)
{
    this->_currentContainer->Padding(padding);

    return *this;
}

ContainerBuilder &ContainerBuilder::Padding(
    int horizontal,
    int vertical)
{
    this->_currentContainer->Padding(horizontal, vertical);

    return *this;
}

ContainerBuilder &ContainerBuilder::Padding(
    int left,
    int top,
    int right,
    int bottom)
{
    this->_currentContainer->Padding(left, top, right, bottom);

    return *this;
}

WidgetBuilder &WidgetBuilder::Font(
    nui::FontStyle *font)
{
    this->_lastCreated->Font(font);

    return *this;
}
