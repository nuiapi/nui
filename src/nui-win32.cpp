#include <nui.h>

#include <glad/glad_wgl.h>

#include <iostream>
#include <map>
#include <memory>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>

// Global variables

// The main window class name.
static WCHAR szWidgetClass[] = L"Widget";

// Forward declarations of functions included in this code module:
LRESULT CALLBACK WidgetProc(
    HWND,
    UINT,
    WPARAM,
    LPARAM);

typedef HGLRC(WINAPI *PFNGLXCREATECONTEXTATTRIBS)(
    HDC hDC,
    HGLRC hShareContext,
    const int *attribList);

/*
 * Font class
 */
namespace nui
{

    namespace priv
    {

        class FontImpl
        {
        public:
            explicit FontImpl(
                const std::wstring &name,
                int size = 14,
                bool bold = false,
                bool italic = false,
                bool underline = false,
                bool striketrough = false);

            virtual ~FontImpl();

            HFONT _hFont;

            std::wstring _name;
            int _size;
            bool _bold;
            bool _italic;
            bool _underline;
            bool _striketrough;
        };

        FontImpl::FontImpl(
            const std::wstring &name,
            int size,
            bool bold,
            bool italic,
            bool underline,
            bool striketrough)
            : _name(name),
              _size(size),
              _bold(bold),
              _italic(italic),
              _underline(underline),
              _striketrough(striketrough)
        {
            this->_hFont = CreateFont(
                this->_size, 0, 0, 0,
                this->_bold ? FW_BOLD : FW_DONTCARE,
                this->_italic ? TRUE : FALSE,
                this->_underline ? TRUE : FALSE,
                this->_striketrough ? TRUE : FALSE,
                ANSI_CHARSET,
                OUT_DEFAULT_PRECIS,
                CLIP_DEFAULT_PRECIS,
                DEFAULT_QUALITY,
                FF_MODERN,
                this->_name.c_str());
        }

        FontImpl::~FontImpl()
        {
            DeleteObject(this->_hFont);
        }

    } // namespace priv

    FontStyle::FontStyle()
        : _impl(new priv::FontImpl(L"Verdana"))
    {}

    FontStyle::FontStyle(
        const FontStyle &style)
        : _impl(nullptr)
    {
        (*this) = style;
    }

    FontStyle::FontStyle(
        const std::wstring &name,
        int size,
        bool bold,
        bool italic,
        bool underline,
        bool striketrough)
        : _impl(new priv::FontImpl(name, size, bold, italic, underline, striketrough))
    {}

    FontStyle::~FontStyle()
    {}

    FontStyle &FontStyle::operator=(
        const FontStyle &other)
    {
        if (this->_impl != nullptr)
        {
            delete this->_impl;
        }

        this->_impl = new priv::FontImpl(other.Name(), other.Size(), other.Bold(), other.Italic(), other.Underline(), other.StrikeTrough());

        return *this;
    }

    FontHandle FontStyle::GetFontHandle() const
    {
        return this->_impl->_hFont;
    }

    std::wstring FontStyle::Name() const
    {
        if (this->_impl == nullptr)
        {
            return L"";
        }

        return this->_impl->_name;
    }

    int FontStyle::Size() const
    {
        if (this->_impl == nullptr)
        {
            return 0;
        }

        return this->_impl->_size;
    }

    bool FontStyle::Bold() const
    {
        if (this->_impl == nullptr)
        {
            return false;
        }

        return this->_impl->_bold;
    }

    bool FontStyle::Italic() const
    {
        if (this->_impl == nullptr)
        {
            return false;
        }

        return this->_impl->_italic;
    }

    bool FontStyle::Underline() const
    {
        if (this->_impl == nullptr)
        {
            return false;
        }

        return this->_impl->_underline;
    }

    bool FontStyle::StrikeTrough() const
    {
        if (this->_impl == nullptr)
        {
            return false;
        }

        return this->_impl->_striketrough;
    }

} // namespace nui

/*
 * ApplicationImpl class
 */
namespace nui
{

    namespace priv
    {

        static std::map<std::wstring, Widget *> widgets;

        class ApplicationImpl
        {
        public:
            ApplicationImpl();

            virtual ~ApplicationImpl();

            HINSTANCE _hInstance;

            WidgetContainer *_topWidget;

            FontStyle _defaultFont;
        };

        ApplicationImpl::ApplicationImpl()
            : _hInstance(nullptr),
              _topWidget(nullptr)
        {}

        ApplicationImpl::~ApplicationImpl()
        {}

    } // namespace priv

} // namespace nui

/*
 * Applciation class
 */
namespace nui
{

    FontStyle *Application::_defaultFont = nullptr;

    Application::Application(
        ApplicationHandle handle)
        : _impl(new priv::ApplicationImpl())
    {
        this->_impl->_hInstance = (HINSTANCE)handle;

        WNDCLASSEX const wcex = {
            sizeof(WNDCLASSEX),
            CS_OWNDC | CS_HREDRAW | CS_VREDRAW,
            WidgetProc,
            0,
            0,
            (HINSTANCE)this->GetApplicationHandle(),
            nullptr,
            LoadCursor(nullptr, IDC_ARROW),
            (HBRUSH)GetStockObject(DC_BRUSH),
            nullptr,
            szWidgetClass,
            nullptr};

        if (RegisterClassEx(&wcex) == FALSE)
        {
            MessageBox(nullptr, _T("Call to RegisterClassEx failed!"), _T("nui::Application"), MB_OK);
        }
    }

    Application::~Application()
    {}

    int Application::Run()
    {
        MSG msg;
        bool running = true;
        bool active = true;

        {
            nui::event::InitEventArgs args(this->_impl->_topWidget);
            this->OnInit(&args);
        }

        while (running)
        {
            while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
            {
                if (msg.message == WM_QUIT)
                {
                    running = false;
                }
                else if (msg.message == WM_ACTIVATE)
                {
                    if (msg.hwnd == this->_impl->_topWidget->Handle())
                    {
                        active = !HIWORD(msg.wParam);
                    }
                }
                else
                {
                    TranslateMessage(&msg);
                    DispatchMessage(&msg);
                }
            }

            if (active)
            {
                event::IdleEventArgs args(this->_impl->_topWidget);
                this->OnIdle(&args);
            }
        }

        {
            nui::event::ExitEventArgs args(this->_impl->_topWidget);
            this->OnExit(&args);
        }

        return (int)msg.wParam;
    }

    ApplicationHandle Application::GetApplicationHandle()
    {
        return this->_impl->_hInstance;
    }

    Widget *Application::Find(
        const std::wstring &id)
    {
        std::map<std::wstring, Widget *>::iterator found = priv::widgets.find(id);

        if (found != priv::widgets.end())
            return found->second;

        return nullptr;
    }

    WidgetContainer *Application::CreateTopWidget(
        nui::Controller *controller,
        const std::wstring &id)
    {
        this->_impl->_topWidget = new WidgetContainer(controller, id, this, 0);

        return this->_impl->_topWidget;
    }

    WidgetContainer *Application::CreateChildContainer(
        nui::Controller *controller,
        const std::wstring &id,
        WidgetContainer *parent)
    {
        WidgetContainer *w = new WidgetContainer(controller, id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    Widget *Application::CreateChildWidget(
        const std::wstring &id,
        WidgetContainer *parent)
    {
        Widget *w = new Widget(id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    GlWidget *Application::CreateGlWidget(
        const std::wstring &id,
        WidgetContainer *parent)
    {
        GlWidget *w = new GlWidget(id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    Label *Application::CreateLabel(
        const std::wstring &id,
        WidgetContainer *parent)
    {
        Label *w = new Label(id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    Button *Application::CreateButton(
        const std::wstring &id,
        WidgetContainer *parent)
    {
        Button *w = new Button(id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    RadioButton *Application::CreateRadioButton(
        const std::wstring &id,
        WidgetContainer *parent)
    {
        RadioButton *w = new RadioButton(id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    CheckBox *Application::CreateCheckBox(
        const std::wstring &id,
        WidgetContainer *parent)
    {
        CheckBox *w = new CheckBox(id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    TextBox *Application::CreateTextBox(
        const std::wstring &id,
        WidgetContainer *parent)
    {
        TextBox *w = new TextBox(id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    ComboBox *Application::CreateComboBox(
        const std::wstring &id,
        WidgetContainer *parent,
        ComboBox::eType type)
    {
        ComboBox *w = new ComboBox(id, this, parent, type);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    ListBox *Application::CreateListBox(
        const std::wstring &id,
        WidgetContainer *parent)
    {
        ListBox *w = new ListBox(id, this, parent);

        if (parent != 0)
        {
            parent->AddChild(w);
        }

        return w;
    }

    void Application::MessagePopUp(
        const std::wstring &message)
    {
        MessageBox(NULL, message.c_str(), L"nui::MessagePopUp", MB_OK);
    }

} // namespace nui

/*
 * WidgetImpl class
 */
namespace nui
{

    namespace priv
    {

        class WidgetImpl
        {
        public:
            explicit WidgetImpl(
                Widget *w);

            WidgetImpl(
                Widget *w,
                DWORD dwExStyle,
                wchar_t *lpClassName,
                wchar_t *lpWindowName,
                DWORD dwStyle);

            virtual ~WidgetImpl();

            std::wstring _id;

            DWORD _dwExStyle;
            wchar_t *_lpClassName;
            wchar_t *_lpWindowName;
            DWORD _dwStyle;

            HWND _hWnd;

            bool _widthFixed;
            bool _heightFixed;

            virtual LRESULT CALLBACK HandleMessages(
                UINT uMsg,
                WPARAM wParam,
                LPARAM lParam);

        protected:
            Widget *_w;
        };

        WidgetImpl::WidgetImpl(
            Widget *w)
            : _dwExStyle(0),
              _lpClassName(szWidgetClass),
              _lpWindowName(L"Widget"),
              _dwStyle(WS_VISIBLE | WS_CLIPCHILDREN),
              _hWnd(nullptr),
              _widthFixed(false),
              _heightFixed(false),
              _w(w)
        {}

        WidgetImpl::WidgetImpl(
            Widget *w,
            DWORD dwExStyle,
            wchar_t *lpClassName,
            wchar_t *lpWindowName,
            DWORD dwStyle)
            : _dwExStyle(dwExStyle),
              _lpClassName(lpClassName),
              _lpWindowName(lpWindowName),
              _dwStyle(dwStyle),
              _hWnd(nullptr),
              _widthFixed(false),
              _heightFixed(false),
              _w(w)
        {}

        WidgetImpl::~WidgetImpl()
        {}

        LRESULT CALLBACK WidgetImpl::HandleMessages(
            UINT uMsg,
            WPARAM wParam,
            LPARAM lParam)
        {
            switch (uMsg)
            {
                case WM_SIZE:
                {
                    WORD nWidth = LOWORD(lParam);
                    WORD nHeight = HIWORD(lParam);

                    nui::event::ResizeEventArgs args(this->_w, nWidth, nHeight);
                    this->_w->OnResize(&args);
                    break;
                }
                case WM_DESTROY:
                {
                    PostQuitMessage(0);
                    break;
                }
            }

            return DefWindowProc(this->_hWnd, uMsg, wParam, lParam);
        }

    } // namespace priv

} // namespace nui

/*
 * Widget class
 */
namespace nui
{

    Widget::Widget(
        const std::wstring &id,
        nui::Application *app,
        nui::WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : _impl(impl != nullptr ? impl : new priv::WidgetImpl(this)),
          _application(app),
          _parent(parent)
    {
        this->_impl->_id = id;
        priv::widgets.insert(std::make_pair(id, this));

        HWND parentHandle = (parent != nullptr ? parent->Handle() : nullptr);
        this->_impl->_hWnd = CreateWindowEx(
            this->_impl->_dwExStyle,
            this->_impl->_lpClassName,
            this->_impl->_lpWindowName,
            this->_impl->_dwStyle | (parentHandle != nullptr ? WS_CHILD : WS_OVERLAPPEDWINDOW),
            CW_USEDEFAULT, CW_USEDEFAULT,
            CW_USEDEFAULT, CW_USEDEFAULT,
            (HWND)parentHandle,
            (HMENU) nullptr,
            (HINSTANCE)app->GetApplicationHandle(),
            (VOID *)this->_impl);

        if (this->_impl->_hWnd == 0)
        {
            DWORD err = GetLastError();
            std::cout << "Unable to create window; Windows gives us an error code: " << err << std::endl;
        }

        if (Application::_defaultFont != nullptr)
            this->Font(Application::_defaultFont);

        ShowWindow(this->_impl->_hWnd, SW_SHOW);
        UpdateWindow(this->_impl->_hWnd);
    }

    Widget::~Widget()
    {
        DestroyWindow(this->Handle());
        priv::widgets.erase(this->Id());
    }

    Application *Widget::App() const
    {
        return this->_application;
    }

    WidgetHandle Widget::Handle() const
    {
        return this->_impl->_hWnd;
    }

    WidgetContainer *Widget::Parent() const
    {
        return this->_parent;
    }

    const Controller *Widget::LinkedController() const
    {
        if (Parent() != nullptr)
        {
            return Parent()->LinkedController();
        }

        return nullptr;
    }

    Controller *Widget::LinkedController()
    {
        if (Parent() != nullptr)
        {
            return Parent()->LinkedController();
        }

        return nullptr;
    }

    const std::wstring &Widget::Id() const
    {
        return this->_impl->_id;
    }

    std::wstring Widget::Text() const
    {
        int len = GetWindowTextLength(this->Handle());

        if (len >= 256)
        {
            auto str = std::make_unique<wchar_t>(len + 1);

            GetWindowText(this->Handle(), (LPWSTR)str.get(), len + 1);

            std::wstring r(str.get());
            return r;
        }

        wchar_t str[256];
        GetWindowText(this->Handle(), (LPWSTR)str, 256);
        return std::wstring(str);
    }

    void Widget::Text(
        const std::wstring &text)
    {
        SetWindowText(this->Handle(), text.c_str());
    }

    bool Widget::IsWidthFixed() const
    {
        return this->_impl->_widthFixed;
    }

    void Widget::Font(
        const FontStyle *font)
    {
        SendMessage(this->Handle(), WM_SETFONT, (WPARAM)(font->GetFontHandle()), TRUE);
    }

    void Widget::SetWidthFixed(
        bool fixed)
    {
        this->_impl->_widthFixed = fixed;
    }

    int Widget::Width() const
    {
        RECT rc;
        GetWindowRect(this->Handle(), &rc);

        return rc.right - rc.left;
    }

    void Widget::Width(
        int width)
    {
        RECT rc;
        GetWindowRect(this->Handle(), &rc);
        MoveWindow(this->Handle(), rc.left, rc.top, width, rc.bottom - rc.top, TRUE);

        this->_impl->_widthFixed = true;
    }

    bool Widget::IsHeightFixed() const
    {
        return this->_impl->_heightFixed;
    }

    void Widget::SetHeightFixed(
        bool fixed)
    {
        this->_impl->_heightFixed = fixed;
    }

    int Widget::Height() const
    {
        RECT rc;
        GetWindowRect(this->Handle(), &rc);

        return rc.bottom - rc.top;
    }

    void Widget::Height(
        int height)
    {
        RECT rc;
        GetWindowRect(this->Handle(), &rc);
        MoveWindow(this->Handle(), rc.left, rc.top, rc.right - rc.left, height, TRUE);

        this->_impl->_heightFixed = true;
    }

} // namespace nui

/*
 * WidgetContainerImpl class
 */
namespace nui
{

    namespace priv
    {

        class WidgetContainerImpl : public WidgetImpl
        {
        public:
            explicit WidgetContainerImpl(
                WidgetContainer *w);

            virtual ~WidgetContainerImpl();

            ChildArrangementStyles::eType _childArrangement;
            int _padding[4];

            WidgetContainer *GetWidget();

            void UpdateChildren();

            void UpdateChildRects(int width, int height);

            virtual LRESULT CALLBACK HandleMessages(
                UINT uMsg,
                WPARAM wParam,
                LPARAM lParam);
        };

        WidgetContainerImpl::WidgetContainerImpl(
            WidgetContainer *w)
            : WidgetImpl(w),
              _childArrangement(ChildArrangementStyles::Horizontal)
        {
            this->_padding[0] = this->_padding[1] = this->_padding[2] = this->_padding[3] = 5;
        }

        WidgetContainerImpl::~WidgetContainerImpl()
        {}

        WidgetContainer *WidgetContainerImpl::GetWidget()
        {
            return (WidgetContainer *)this->_w;
        }

        void WidgetContainerImpl::UpdateChildren()
        {
            RECT rc;
            GetClientRect(this->_w->Handle(), &rc);
            this->UpdateChildRects(rc.right - rc.left, rc.bottom - rc.top);
        }

        void WidgetContainerImpl::UpdateChildRects(
            int width,
            int height)
        {
            const std::vector<Widget *> &children = ((WidgetContainer *)this->_w)->Children();
            auto size = children.size();
            if (size > 0)
            {
                if (this->_childArrangement == ChildArrangementStyles::Horizontal)
                {
                    int fixed = 0, fixedCount = 0;
                    for (std::vector<nui::Widget *>::const_iterator i = children.begin(); i != children.end(); ++i)
                        if ((*i)->IsWidthFixed())
                        {
                            fixed += (*i)->Width() + (this->_padding[0] + this->_padding[2]);
                            fixedCount++;
                        }

                    auto childWidth = (width - fixed) / (children.size() - fixedCount);
                    int x = 0;
                    for (std::vector<nui::Widget *>::const_iterator i = children.begin(); i != children.end(); ++i)
                    {
                        Widget *widget = (Widget *)*i;
                        if (widget->IsWidthFixed() == false)
                        {
                            MoveWindow(widget->Handle(), x + this->_padding[0], this->_padding[1], int(childWidth) - (this->_padding[0] + this->_padding[2]), height - (this->_padding[1] + this->_padding[3]), TRUE);
                            x += int(childWidth);
                        }
                        else
                        {
                            MoveWindow(widget->Handle(), x + this->_padding[0], this->_padding[1], widget->Width(), height - (this->_padding[1] + this->_padding[3]), TRUE);
                            x += widget->Width() + this->_padding[0] + this->_padding[2];
                        }
                    }
                }
                else if (this->_childArrangement == ChildArrangementStyles::Vertical)
                {
                    int fixed = 0, fixedCount = 0;
                    for (std::vector<nui::Widget *>::const_iterator i = children.begin(); i != children.end(); ++i)
                        if ((*i)->IsHeightFixed())
                        {
                            fixed += (*i)->Height() + (this->_padding[1] + this->_padding[3]);
                            fixedCount++;
                        }

                    auto childHeight = (height - fixed) / (children.size() - fixedCount);
                    int y = 0;
                    for (std::vector<nui::Widget *>::const_iterator i = children.begin(); i != children.end(); ++i)
                    {
                        Widget *widget = (Widget *)*i;
                        if (widget->IsHeightFixed() == false)
                        {
                            MoveWindow(widget->Handle(), this->_padding[0], y + this->_padding[1], width - (this->_padding[0] + this->_padding[2]), int(childHeight) - (this->_padding[1] + this->_padding[3]), TRUE);
                            y += int(childHeight);
                        }
                        else
                        {
                            MoveWindow(widget->Handle(), this->_padding[0], y + this->_padding[1], width - (this->_padding[0] + this->_padding[2]), widget->Height(), TRUE);
                            y += widget->Height() + this->_padding[1] + this->_padding[3];
                        }
                    }
                }
            }
        }

        LRESULT CALLBACK WidgetContainerImpl::HandleMessages(
            UINT uMsg,
            WPARAM wParam,
            LPARAM lParam)
        {
            switch (uMsg)
            {
                case WM_SIZE:
                {
                    this->UpdateChildRects(LOWORD(lParam), HIWORD(lParam));
                    RECT rc;
                    GetWindowRect(this->_hWnd, &rc);
                    InvalidateRect(this->_hWnd, &rc, TRUE);
                    break;
                }
                case WM_DESTROY:
                {
                    PostQuitMessage(0);
                    break;
                }
                case WM_COMMAND:
                {
                    std::vector<Widget *> &children = this->GetWidget()->Children();
                    for (std::vector<Widget *>::iterator i = children.begin(); i != children.end(); ++i)
                    {
                        if ((HWND)lParam == (HWND)(*i)->Handle())
                        {
                            if (HIWORD(wParam) == BN_CLICKED)
                            {
                                nui::event::ClickEventArgs args(*i);

                                Button *b = dynamic_cast<Button *>(*i);
                                RadioButton *r = dynamic_cast<RadioButton *>(*i);
                                CheckBox *c = dynamic_cast<CheckBox *>(*i);
                                if (b != nullptr)
                                    b->OnClick(&args);
                                else if (r != nullptr)
                                    r->OnClick(&args);
                                else if (c != nullptr)
                                    c->OnClick(&args);
                            }
                            else if (HIWORD(wParam) == LBN_SELCHANGE)
                            {
                                ListBox *l = dynamic_cast<ListBox *>(*i);

                                if (l != nullptr)
                                {
                                    nui::event::SelectedIndexChangedEventArgs args(*i, l->SelectedIndex());
                                    l->OnSelectedIndexChanged(&args);
                                }
                            }
                            else if (HIWORD(wParam) == CBN_SELCHANGE)
                            {
                                ComboBox *c = dynamic_cast<ComboBox *>(*i);

                                if (c != nullptr)
                                {
                                    nui::event::SelectedIndexChangedEventArgs args(*i, c->SelectedIndex());
                                    c->OnSelectedIndexChanged(&args);
                                }
                            }
                        }
                    }
                    break;
                }
            }

            return WidgetImpl::HandleMessages(uMsg, wParam, lParam);
        }

    } // namespace priv

} // namespace nui

/*
 * WidgetContainer class
 */
namespace nui
{

    WidgetContainer::WidgetContainer(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetContainerImpl(this))
    {
        ((priv::WidgetContainerImpl *)this->_impl)->UpdateChildren();
    }

    WidgetContainer::WidgetContainer(
        Controller *linkedController,
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetContainerImpl(this)),
          _linkedController(linkedController)
    {
        ((priv::WidgetContainerImpl *)this->_impl)->UpdateChildren();
    }

    WidgetContainer::~WidgetContainer()
    {}

    const Controller *WidgetContainer::LinkedController() const
    {
        if (_linkedController == nullptr && Parent() != nullptr)
        {
            return Parent()->LinkedController();
        }

        return _linkedController;
    }

    Controller *WidgetContainer::LinkedController()
    {
        if (_linkedController == nullptr && Parent() != nullptr)
        {
            return Parent()->LinkedController();
        }

        return _linkedController;
    }

    const std::vector<Widget *> &WidgetContainer::Children() const
    {
        return this->_children;
    }

    std::vector<Widget *> &WidgetContainer::Children()
    {
        return this->_children;
    }

    void WidgetContainer::AddChild(
        Widget *widget)
    {
        this->_children.push_back(widget);
        ((priv::WidgetContainerImpl *)this->_impl)->UpdateChildren();
    }

    void WidgetContainer::RemoveChild(
        Widget *widget)
    {
        for (std::vector<Widget *>::iterator i = this->_children.begin(); i != this->_children.end(); ++i)
        {
            if (*i == widget)
            {
                this->_children.erase(i);
                break;
            }
        }

        ((priv::WidgetContainerImpl *)this->_impl)->UpdateChildren();
    }

    Widget *WidgetContainer::FindChildById(
        const std::wstring &id)
    {
        for (auto child : _children)
        {
            if (child->Id() == id)
            {
                return child;
            }
        }

        return nullptr;
    }

    void WidgetContainer::ChildArrangementStyle(
        ChildArrangementStyles::eType style)
    {
        ((priv::WidgetContainerImpl *)this->_impl)->_childArrangement = style;
        ((priv::WidgetContainerImpl *)this->_impl)->UpdateChildren();
    }

    ChildArrangementStyles::eType WidgetContainer::ChildArrangementStyle()
    {
        return ((priv::WidgetContainerImpl *)this->_impl)->_childArrangement;
    }

    int *WidgetContainer::Padding() const
    {
        return ((priv::WidgetContainerImpl *)this->_impl)->_padding;
    }

    void WidgetContainer::Padding(
        int padding)
    {
        ((priv::WidgetContainerImpl *)this->_impl)->_padding[0] =
            ((priv::WidgetContainerImpl *)this->_impl)->_padding[1] =
                ((priv::WidgetContainerImpl *)this->_impl)->_padding[2] =
                    ((priv::WidgetContainerImpl *)this->_impl)->_padding[3] = padding;
        ((priv::WidgetContainerImpl *)this->_impl)->UpdateChildren();
    }

    void WidgetContainer::Padding(
        int horizontal,
        int vertical)
    {
        ((priv::WidgetContainerImpl *)this->_impl)->_padding[0] =
            ((priv::WidgetContainerImpl *)this->_impl)->_padding[2] = horizontal;
        ((priv::WidgetContainerImpl *)this->_impl)->_padding[1] =
            ((priv::WidgetContainerImpl *)this->_impl)->_padding[3] = vertical;

        ((priv::WidgetContainerImpl *)this->_impl)->UpdateChildren();
    }

    void WidgetContainer::Padding(
        int left,
        int top,
        int right,
        int bottom)
    {
        ((priv::WidgetContainerImpl *)this->_impl)->_padding[0] = left;
        ((priv::WidgetContainerImpl *)this->_impl)->_padding[1] = top;
        ((priv::WidgetContainerImpl *)this->_impl)->_padding[2] = right;
        ((priv::WidgetContainerImpl *)this->_impl)->_padding[3] = bottom;
        ((priv::WidgetContainerImpl *)this->_impl)->UpdateChildren();
    }

} // namespace nui

/*
 * GlWidget class
 */
namespace nui
{

    namespace priv
    {

        class GlWidgetImpl : public WidgetImpl
        {
        public:
            explicit GlWidgetImpl(
                Widget *w);

            virtual ~GlWidgetImpl();

            HDC _hDC;
            HGLRC _hRC;

            bool SetupGl();
            bool ReplaceWithModernGl(
                int major,
                int minor);

            LRESULT CALLBACK HandleMessages(
                UINT uMsg,
                WPARAM wParam,
                LPARAM lParam) override;
        };

        GlWidgetImpl::GlWidgetImpl(
            Widget *w)
            : WidgetImpl(w, 0, szWidgetClass, L"OpenGlWidget", WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN),
              _hDC(nullptr),
              _hRC(nullptr)
        {}

        GlWidgetImpl::~GlWidgetImpl()
        {}

        bool GlWidgetImpl::SetupGl()
        {
            this->_hDC = GetDC(this->_hWnd);

            if (this->_hDC != nullptr)
            {
                PIXELFORMATDESCRIPTOR const pfd = {
                    sizeof(PIXELFORMATDESCRIPTOR),
                    1,
                    PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
                    PFD_TYPE_RGBA,
                    32, // Color bits
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    16, // Depth bits
                    0, 0,
                    PFD_MAIN_PLANE,
                    0, 0, 0, 0};

                auto const pixelFormat = ChoosePixelFormat(this->_hDC, &pfd);

                if (pixelFormat)
                {
                    if (::SetPixelFormat(this->_hDC, pixelFormat, &pfd) != false)
                    {
                        this->_hRC = wglCreateContext(this->_hDC);
                        if (this->_hRC != nullptr)
                        {
                            wglMakeCurrent(this->_hDC, this->_hRC);

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        bool GlWidgetImpl::ReplaceWithModernGl(
            int major,
            int minor)
        {
            wglMakeCurrent(this->_hDC, this->_hRC);

            static const int attribList[] = {
                WGL_CONTEXT_MAJOR_VERSION_ARB, major,
                WGL_CONTEXT_MINOR_VERSION_ARB, minor,
                0};

            if (gladLoadGL() && gladLoadWGL(this->_hDC))
            {
                auto const hTmpRC = wglCreateContextAttribsARB(this->_hDC, 0, attribList);
                if (hTmpRC != nullptr)
                {
                    wglMakeCurrent(nullptr, nullptr);
                    wglDeleteContext(this->_hRC);
                    this->_hRC = hTmpRC;
                    wglMakeCurrent(this->_hDC, this->_hRC);

                    return true;
                }
            }

            return false;
        }

        LRESULT CALLBACK GlWidgetImpl::HandleMessages(
            UINT uMsg,
            WPARAM wParam,
            LPARAM lParam)
        {
            switch (uMsg)
            {
                case WM_SIZE:
                {
                    break;
                }
            }

            return WidgetImpl::HandleMessages(uMsg, wParam, lParam);
        }

    } // namespace priv

    GlWidget::GlWidget(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::GlWidgetImpl(this))
    {
        // We need this here instead of in the Impl class because there the window is not yet created (DUH!)
        ((priv::GlWidgetImpl *)this->_impl)->SetupGl();
    }

    GlWidget::~GlWidget()
    {}

    bool GlWidget::SetupModernGl(
        int major,
        int minor)
    {
        return ((priv::GlWidgetImpl *)this->_impl)->ReplaceWithModernGl(major, minor);
    }

    void GlWidget::BeginFrame()
    {
        wglMakeCurrent(((priv::GlWidgetImpl *)this->_impl)->_hDC, ((priv::GlWidgetImpl *)this->_impl)->_hRC);
    }

    void GlWidget::EndFrame()
    {
        SwapBuffers(((priv::GlWidgetImpl *)this->_impl)->_hDC);
    }

} // namespace nui

/*
 * Label class
 */
namespace nui
{

    Label::Label(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetImpl(this, 0, L"STATIC", L"Static control", WS_VISIBLE))
    {}

    Label::~Label()
    {}

} // namespace nui

/*
 * Button class
 */
namespace nui
{

    Button::Button(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetImpl(this, 0, L"BUTTON", L"Button control", WS_TABSTOP | WS_VISIBLE | BS_PUSHBUTTON | BS_FLAT))
    {}

    Button::~Button()
    {}

} // namespace nui

/*
 * RadioButton class
 */
namespace nui
{

    RadioButton::RadioButton(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetImpl(this, 0, L"BUTTON", L"RadioButton control", WS_TABSTOP | WS_VISIBLE | BS_AUTORADIOBUTTON | BS_FLAT))
    {}

    RadioButton::~RadioButton()
    {}

    bool RadioButton::IsChecked() const
    {
        auto const res = SendMessage(this->Handle(), BM_GETCHECK, 0, 0);

        return (res == BST_CHECKED);
    }

    void RadioButton::Check(
        bool enabled)
    {
        SendMessage(this->Handle(), BM_SETCHECK, (WPARAM)(enabled ? BST_CHECKED : BST_UNCHECKED), 0);
    }

} // namespace nui

/*
 * CheckBox class
 */
namespace nui
{

    CheckBox::CheckBox(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetImpl(this, 0, L"BUTTON", L"CheckBox control", WS_TABSTOP | WS_VISIBLE | BS_AUTOCHECKBOX | BS_FLAT))
    {}

    CheckBox::~CheckBox()
    {}

    bool CheckBox::IsChecked() const
    {
        auto const res = SendMessage(this->Handle(), BM_GETCHECK, 0, 0);

        return (res == BST_CHECKED);
    }

    void CheckBox::Check(
        bool enabled)
    {
        SendMessage(this->Handle(), BM_SETCHECK, (WPARAM)(enabled ? BST_CHECKED : BST_UNCHECKED), 0);
    }

} // namespace nui

/*
 * TextBox class
 */
namespace nui
{

    TextBox::TextBox(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetImpl(this, 0, L"EDIT", L"TextBox control", WS_TABSTOP | WS_VISIBLE | WS_VSCROLL | ES_MULTILINE))
    {}

    TextBox::~TextBox()
    {}

} // namespace nui

/*
 * ComboBox class
 */
namespace nui
{

    ComboBox::ComboBox(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        ComboBox::eType type,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetImpl(this, 0, L"COMBOBOX", L"ComboBox control", WS_TABSTOP | WS_VISIBLE | (type == ComboBox::Simple ? CBS_SIMPLE : (type == ComboBox::DropDown ? CBS_DROPDOWN : CBS_DROPDOWNLIST))))
    {}

    ComboBox::~ComboBox()
    {}

    void ComboBox::SetList(
        const std::vector<std::wstring> &items)
    {
        for (auto &item : items)
        {
            SendMessage(this->Handle(), CB_ADDSTRING, 0, (LPARAM)item.c_str());
        }
    }

    void ComboBox::AddListItem(
        const std::wstring &text)
    {
        SendMessage(this->Handle(), CB_ADDSTRING, 0, (LPARAM)text.c_str());
    }

    void ComboBox::InsertListItem(
        int index,
        const std::wstring &text)
    {
        SendMessage(this->Handle(), CB_INSERTSTRING, (WPARAM)index, (LPARAM)text.c_str());
    }

    void ComboBox::RemoveListItem(
        int index)
    {
        SendMessage(this->Handle(), CB_DELETESTRING, (WPARAM)index, 0);
    }

    void ComboBox::ClearItems()
    {
        SendMessage(this->Handle(), CB_RESETCONTENT, 0, 0);
    }

    int ComboBox::IndexOf(
        const std::wstring &text,
        int from)
    {
        return int(SendMessage(this->Handle(), CB_FINDSTRINGEXACT, (WPARAM)from, (LPARAM)text.c_str()));
    }

    int ComboBox::SelectedIndex() const
    {
        return int(SendMessage(this->Handle(), CB_GETCURSEL, 0, 0));
    }

    std::wstring ComboBox::SelectedText() const
    {
        int index = this->SelectedIndex();
        int len = (int)SendMessage(this->Handle(), CB_GETLBTEXTLEN, (WPARAM)index, 0);
        if (len > 0)
        {
            auto c = std::make_unique<wchar_t>(len + 1);
            SendMessage(this->Handle(), CB_GETLBTEXT, (WPARAM)index, (LPARAM)c.get());
            std::wstring result(c.get());
            return result;
        }

        return std::wstring(L"");
    }

} // namespace nui

/*
 * ListBox class
 */
namespace nui
{

    ListBox::ListBox(
        const std::wstring &id,
        Application *app,
        WidgetContainer *parent,
        priv::WidgetImpl *impl)
        : Widget(id, app, parent, impl != nullptr ? impl : new priv::WidgetImpl(this, 0, L"listbox", L"ListBox control", LBS_STANDARD | LBS_NOTIFY | WS_TABSTOP | WS_VISIBLE))
    {}

    ListBox::~ListBox()
    {}

    void ListBox::SetList(
        const std::vector<std::wstring> &items)
    {
        for (auto &item : items)
        {
            SendMessage(this->Handle(), LB_ADDSTRING, 0, (LPARAM)item.c_str());
        }
    }

    void ListBox::AddListItem(
        const std::wstring &text)
    {
        SendMessage(this->Handle(), LB_ADDSTRING, 0, (LPARAM)text.c_str());
    }

    void ListBox::InsertListItem(
        int index,
        const std::wstring &text)
    {
        SendMessage(this->Handle(), LB_INSERTSTRING, (WPARAM)index, (LPARAM)text.c_str());
    }

    void ListBox::RemoveListItem(
        int index)
    {
        SendMessage(this->Handle(), LB_DELETESTRING, (WPARAM)index, 0);
    }

    void ListBox::ClearItems()
    {
        SendMessage(this->Handle(), LB_RESETCONTENT, 0, 0);
    }

    int ListBox::IndexOf(
        const std::wstring &text,
        int from)
    {
        return int(SendMessage(this->Handle(), LB_FINDSTRINGEXACT, (WPARAM)from, (LPARAM)text.c_str()));
    }

    int ListBox::SelectedIndex() const
    {
        return (int)SendMessage(this->Handle(), LB_GETCURSEL, 0, 0);
    }

    std::wstring ListBox::SelectedText() const
    {
        int index = this->SelectedIndex();
        int len = (int)SendMessage(this->Handle(), LB_GETTEXTLEN, (WPARAM)index, 0);
        if (len > 0)
        {
            auto c = std::make_unique<wchar_t>(len + 1);
            SendMessage(this->Handle(), LB_GETTEXT, (WPARAM)index, (LPARAM)c.get());
            std::wstring result(c.get());
            return result;
        }

        return std::wstring(L"");
    }

} // namespace nui

LRESULT CALLBACK WidgetProc(
    HWND hWnd,
    UINT uMsg,
    WPARAM wParam,
    LPARAM lParam)
{
    nui::priv::WidgetImpl *widget = 0;
    if (uMsg == WM_NCCREATE)
    {
        widget = reinterpret_cast<nui::priv::WidgetImpl *>(((LPCREATESTRUCT)lParam)->lpCreateParams);

        if (widget != nullptr)
        {
            SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<long long>(widget));

            widget->_hWnd = hWnd;
            return widget->HandleMessages(uMsg, wParam, lParam);
        }
    }
    else
    {
        widget = reinterpret_cast<nui::priv::WidgetImpl *>(::GetWindowLongPtr(hWnd, GWLP_USERDATA));

        if (widget != nullptr && widget->_hWnd == hWnd)
        {
            return widget->HandleMessages(uMsg, wParam, lParam);
        }
    }

    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}
