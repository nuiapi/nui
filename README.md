# Native UI API #

Native UI API to make your live easier writing c++ applications with a Win32 UI. The API is supposed to be simple, so not all features of Win32 will be supported. Current supported widgets are:

* Label (static control)
* Button
* Radiobutton
* CheckBox
* TextBox
* ComboBox (DropDown and DropDownList)
* ListBox
* GlWidget (OpenGL rendering widget)

Future feature is expanding to support unix X11 with the same API and maybe rendering controls in openGL.

On top of the basic API, I made a [Fluent](https://en.wikipedia.org/wiki/Fluent_interface) API to build your UI in a one liner. The following example will start a empty Widget:

	int WINAPI WinMain(HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPSTR lpCmdLine,
					   int nCmdShow)
	{
		return FluiNui(hInstance)
			.MainWindow("MainWidget", "Title of the Main Widget")
			.Run();
	}

